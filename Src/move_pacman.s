.globl move_pacman
 .p2align 2
 .type move_pacman,%function
 .syntax unified
move_pacman: // Function "move_pacman" entry point
 .fnstart
 push {r4-r8, r10, r11, lr}

LDR r4, [r0]		//pacmanx
LDR r5, [r1]		//pacmany
ADD r6, r4, r2	//r6 = newx
ADD r7, r5, r3	//r7 = newy

CMP r6, #0		//begin check for passages
itt lt
MOVLT r6, #27
MOVLT r7, #17
CMP r6, #27
itt gt
MOVgt r6, #0
MOVgt r7, #17	//end check for passages

MOV r9, #28
MLA r10, r7, r9, r6
LDR r8, =screen
ADD r10, r10, r8
LDRB r11, [r10]
CMP r11, #1		//check for walls; NOTE: r11 is also used at the end to return either 7,8 or 0 to the pacman_ate variable to show if a small, big or no dot was eaten
it eq
MOVEQ r0, #0
BEQ	stp

MLA r10, r5, r9, r4
ADD r10, r10, r8
MOV r12, #0
STRB r12, [r10]

//let r10 contain direction
CMP r6, r4
it gt
MOVGT r10, #4
it lt
MOVLT r10, #3
CMP r7, r5
it gt
MOVGT r10, #6
it lt
MOVLT r10, #5

//check direction through tunnels
MOV r12, r10	//keep old direction
CMP r6, #0
it eq
MOVEQ r12, #4
CMP  r4, #27
it eq
MOVEQ r10, r12

MOV r12, r10	//keep old direction
CMP r6, #27
it eq
MOVEQ r12, #3
CMP  r4, #0
it eq
MOVEQ r10, r12
//end check tunnels

MOV r4, r6	//pacmanx = newx
MOV r5, r7	//pacmany = newy
STR r4, [r0]
STR r5, [r1]

MLA r6, r5, r9, r4
ADD r6, r6, r8
STRB r10, [r6]

MOV r0, r11
stp:

 pop {r4-r8, r10, r11, pc}
.fnend
