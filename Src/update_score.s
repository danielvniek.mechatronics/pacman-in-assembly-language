.globl update_score
 .p2align 2
 .type update_score,%function
 .syntax unified
update_score: // Function "update_score" entry point
 .fnstart
push {r1-r8, r10, r11, lr}
MOV r2, r0
MOV r4, #10
LDR r1, =screen
ADD r1, r1, #33
loop:
UDIV r5, r2, r4
MUL r3, r5, r4
SUB r3, r2, r3
ADD r3, r3, #48
STRB r3, [r1]
SUB r1, r1, #1
MOV r2, r5
CMP r2, #0
BGT loop
pop {r1-r8, r10, r11, pc}
.fnend
