
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "usb_device.h"
#include "stm32f411e_discovery_accelerometer.h"

/* USER CODE BEGIN Includes */
#include "usbd_cdc_if.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

I2S_HandleTypeDef hi2s2;
I2S_HandleTypeDef hi2s3;

SPI_HandleTypeDef hspi1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/


// tiles
// 0 black background
// 1 solid wall
// 2 pacman round
// 3 pacman left
// 4 pacman right
// 5 pacman top
// 6 pacman bottom
// 7 small dot
// 8 big dot
// 9 cherry
// 10 bad guy 1
// 11 bad guy 2
// 12 bad guy 3
// 13 bad guy 4


#define SCREEN_LEN 1008
#define SCREEN_WIDTH 28
#define SCREEN_HEIGHT 36
uint8_t screen[1008] = {
// screen is 28 * 36
//0                                       1                                       2
//0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
  0,  0,  0, '1','U','P', 0,  0,  0, 'H','I','G','H', 0 ,'S','C','O','R','E', 0,  0,  0,  0,  0,  0,  0,  0,  0,  //0
  0,  0,  0,  0,  0,  0,  0, 0,  0,  0,  0,  0,  0,  0,  0, '0','0', 0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //1
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //2
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  //3
  1,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  1,  1,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  1,  //4
  1,  7,  1,  1,  1,  1,  7,  1,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  1,  7,  1,  1,  1,  1,  7,  1,  //5
  1,  8,  1,  1,  1,  1,  7,  1,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  1,  7,  1,  1,  1,  1,  8,  1,  //6
  1,  7,  1,  1,  1,  1,  7,  1,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  1,  7,  1,  1,  1,  1,  7,  1,  //7
  1,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  1,  //8
  1,  7,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  7,  1,  //9
  1,  7,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  7,  1,  //10
  1,  7,  7,  7,  7,  7,  7,  1,  1,  7,  7,  7,  7,  1,  1,  7,  7,  7,  7,  1,  1,  7,  7,  7,  7,  7,  7,  1,  //11
  1,  1,  1,  1,  1,  1,  7,  1,  1,  1,  1,  1,  0,  1,  1,  0,  1,  1,  1,  1,  1,  7,  1,  1,  1,  1,  1,  1,  //12
  0,  0,  0,  0,  0,  1,  7,  1,  1,  1,  1,  1,  0,  1,  1,  0,  1,  1,  1,  1,  1,  7,  1,  0,  0,  0,  0,  0,  //13
  0,  0,  0,  0,  0,  1,  7,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  7,  1,  0,  0,  0,  0,  0,  //14
  0,  0,  0,  0,  0,  1,  7,  1,  1,  0,  1,  1,  1,  1,  1,  1,  1,  1,  0,  1,  1,  7,  1,  0,  0,  0,  0,  0,  //15
  1,  1,  1,  1,  1,  1,  7,  1,  1,  0,  1,  0,  0,  0,  0,  0,  0,  1,  0,  1,  1,  7,  1,  1,  1,  1,  1,  1,  //16
  0,  0,  0,  0,  0,  0,  7,  0,  0,  0,  1,  0,  0,  0,  0,  0,  0,  1,  0,  0,  0,  7,  0,  0,  0,  0,  0,  0,  //17
  1,  1,  1,  1,  1,  1,  7,  1,  1,  0,  1,  0,  0,  0,  0,  0,  0,  1,  0,  1,  1,  7,  1,  1,  1,  1,  1,  1,  //18
  0,  0,  0,  0,  0,  1,  7,  1,  1,  0,  1,  1,  1,  1,  1,  1,  1,  1,  0,  1,  1,  7,  1,  0,  0,  0,  0,  0,  //19
  0,  0,  0,  0,  0,  1,  7,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  7,  1,  0,  0,  0,  0,  0,  //20
  0,  0,  0,  0,  0,  1,  7,  1,  1,  0,  1,  1,  1,  1,  1,  1,  1,  1,  0,  1,  1,  7,  1,  0,  0,  0,  0,  0,  //21
  1,  1,  1,  1,  1,  1,  7,  1,  1,  0,  1,  1,  1,  1,  1,  1,  1,  1,  0,  1,  1,  7,  1,  1,  1,  1,  1,  1,  //22
  1,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  1,  1,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  1,  //23
  1,  7,  1,  1,  1,  1,  7,  1,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  1,  7,  1,  1,  1,  1,  7,  1,  //24
  1,  7,  1,  1,  1,  1,  7,  1,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  1,  7,  1,  1,  1,  1,  7,  1,  //25
  1,  8,  7,  7,  1,  1,  7,  7,  7,  7,  7,  7,  7,  2,  7,  7,  7,  7,  7,  7,  7,  7,  1,  1,  7,  7,  8,  1,  //26
  1,  1,  1,  7,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  7,  1,  1,  1,  //27
  1,  1,  1,  7,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  7,  1,  1,  1,  //28
  1,  7,  7,  7,  7,  7,  7,  1,  1,  7,  7,  7,  7,  1,  1,  7,  7,  7,  7,  1,  1,  7,  7,  7,  7,  7,  7,  1,  //29
  1,  7,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  7,  1,  //30
  1,  7,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  7,  1,  1,  7,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  7,  1,  //31
  1,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  7,  1,  //32
  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  //33
  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  //34
  0,  0,  0,  3,  0,  3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  9,  0,  0,  0   //35

};

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_I2S2_Init(void);
static void MX_I2S3_Init(void);
static void MX_SPI1_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
uint8_t move_pacman(int32_t* ptr_pacmanx, int32_t* ptr_pacmany, int32_t dx, int32_t dy);
//{
//	// get x,y location that was passed as references (addresses)
//	int32_t pacmanx = *ptr_pacmanx;
//	int32_t pacmany = *ptr_pacmany;
//
//	int32_t newx = pacmanx+dx;
//	int32_t newy = pacmany+dy;
//
//	if (newx < 0){
//		newx = 27;
//		newy = 17;
//	}
//	if (newx > 27){
//		newx = 0;
//		newy = 17;
//	}
//	if(screen[newy*28 + newx]==1){
//		return 0;
//	}
//	uint8_t points=0;
//	if(screen[newy*28 + newx]==7){
//		points = 7;
//	}
//	if(screen[newy*28 + newx]==8){
//		points = 8;
//	}
//
//	// erase pacman at the current position
//	screen[pacmany * 28 + pacmanx] = 0;
//	//change face direction
//	int32_t direction = 2;
//	if ((newx>pacmanx)||((newx==0)&&(pacmanx==27)))
//		direction = 4;
//	if ((newx<pacmanx)||((newx==27)&&(pacmanx==0)))
//			direction = 3;
//	if (newy>pacmany)
//			direction = 6;
//	if (newy<pacmany)
//			direction = 5;
//	// update pacman x,y position
//	pacmanx = newx;
//	pacmany = newy;
//
//	// check that we don't go out of the screen
//
//
//	// draw pacman at the new position
//	screen[pacmany * 28 + pacmanx] = direction;
//
//	// store updated x,y location back to ptr_pacmanx and ptr_pacmany
//	*ptr_pacmanx = pacmanx;
//	*ptr_pacmany = pacmany;
//
//	return points;
//}
// update score on screen
void update_score(uint32_t score);
//{
//	uint32_t SC = score;
//	uint32_t value = 0;
//	int32_t i = 0;
//
//	while(SC>0){
//		value = SC%10;
//		screen[33-i]=value+48;
//		SC = SC/10;
//		i++;
//
//	}
//
//}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  *
  * @retval None
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
   int32_t absx;
   int32_t absy;
   int32_t dx;
   int32_t dy;
   int16_t accelero_buffer[3];
   int32_t pacmanx;
   int32_t pacmany;
   uint8_t pacman_ate;
   uint32_t score = 0;
  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_I2S2_Init();
  MX_I2S3_Init();
  MX_SPI1_Init();
  MX_USB_DEVICE_Init();

  /* USER CODE BEGIN 2 */

  BSP_ACCELERO_Init();
  pacmanx = 13;
  pacmany = 26;

  update_score(0);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

	 CDC_Transmit_FS(screen, SCREEN_LEN);
	 HAL_Delay(100);

     BSP_ACCELERO_GetXYZ(accelero_buffer);

     absx = 0;
     absy = 0;
     dx = 0;
     dy = 0;
     if (accelero_buffer[0] > 4000)
     {
    	 absx = accelero_buffer[0];
    	 dx = 1;
     }
     else if (accelero_buffer[0] < -4000)
     {
    	 absx = -accelero_buffer[0];
    	 dx = -1;
     }

     if (accelero_buffer[1] > 4000)
     {
    	 absy = accelero_buffer[1];
    	 dy = -1;
     }
     else if (accelero_buffer[1] < -4000)
     {
    	 absy = -accelero_buffer[1];
    	 dy = 1;
     }

     if (absy > absx)
    	 dx = 0;
     else
    	 dy = 0;

     if ((dx != 0) || (dy != 0))
     {
    	 pacman_ate = move_pacman(&pacmanx, &pacmany, dx, dy);
    	 if (pacman_ate == 7) //small dot
    	 {
    		 score += 10;
    		 update_score(score);
    	 }
    	 else if (pacman_ate == 8)
    	 {

    	 }
     }

  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */

}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct;

    /**Configure the main internal regulator output voltage 
    */
  __HAL_RCC_PWR_CLK_ENABLE();

  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 192;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV4;
  RCC_OscInitStruct.PLL.PLLQ = 8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_I2S;
  PeriphClkInitStruct.PLLI2S.PLLI2SN = 200;
  PeriphClkInitStruct.PLLI2S.PLLI2SM = 5;
  PeriphClkInitStruct.PLLI2S.PLLI2SR = 2;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* I2C1 init function */
static void MX_I2C1_Init(void)
{

  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2S2 init function */
static void MX_I2S2_Init(void)
{

  hi2s2.Instance = SPI2;
  hi2s2.Init.Mode = I2S_MODE_MASTER_TX;
  hi2s2.Init.Standard = I2S_STANDARD_PHILIPS;
  hi2s2.Init.DataFormat = I2S_DATAFORMAT_16B;
  hi2s2.Init.MCLKOutput = I2S_MCLKOUTPUT_DISABLE;
  hi2s2.Init.AudioFreq = I2S_AUDIOFREQ_96K;
  hi2s2.Init.CPOL = I2S_CPOL_LOW;
  hi2s2.Init.ClockSource = I2S_CLOCK_PLL;
  hi2s2.Init.FullDuplexMode = I2S_FULLDUPLEXMODE_ENABLE;
  if (HAL_I2S_Init(&hi2s2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* I2S3 init function */
static void MX_I2S3_Init(void)
{

  hi2s3.Instance = SPI3;
  hi2s3.Init.Mode = I2S_MODE_MASTER_TX;
  hi2s3.Init.Standard = I2S_STANDARD_PHILIPS;
  hi2s3.Init.DataFormat = I2S_DATAFORMAT_16B;
  hi2s3.Init.MCLKOutput = I2S_MCLKOUTPUT_ENABLE;
  hi2s3.Init.AudioFreq = I2S_AUDIOFREQ_96K;
  hi2s3.Init.CPOL = I2S_CPOL_LOW;
  hi2s3.Init.ClockSource = I2S_CLOCK_PLL;
  hi2s3.Init.FullDuplexMode = I2S_FULLDUPLEXMODE_DISABLE;
  if (HAL_I2S_Init(&hi2s3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* SPI1 init function */
static void MX_SPI1_Init(void)
{

  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  GPIO_InitTypeDef GPIO_InitStruct;

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CS_I2C_SPI_GPIO_Port, CS_I2C_SPI_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(OTG_FS_PowerSwitchOn_GPIO_Port, OTG_FS_PowerSwitchOn_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, LD4_Pin|LD3_Pin|LD5_Pin|LD6_Pin 
                          |Audio_RST_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : DATA_Ready_Pin */
  GPIO_InitStruct.Pin = DATA_Ready_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(DATA_Ready_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : CS_I2C_SPI_Pin */
  GPIO_InitStruct.Pin = CS_I2C_SPI_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(CS_I2C_SPI_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : INT1_Pin INT2_Pin MEMS_INT2_Pin */
  GPIO_InitStruct.Pin = INT1_Pin|INT2_Pin|MEMS_INT2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : OTG_FS_PowerSwitchOn_Pin */
  GPIO_InitStruct.Pin = OTG_FS_PowerSwitchOn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(OTG_FS_PowerSwitchOn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : PA0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_EVT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : LD4_Pin LD3_Pin LD5_Pin LD6_Pin 
                           Audio_RST_Pin */
  GPIO_InitStruct.Pin = LD4_Pin|LD3_Pin|LD5_Pin|LD6_Pin 
                          |Audio_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pin : OTG_FS_OverCurrent_Pin */
  GPIO_InitStruct.Pin = OTG_FS_OverCurrent_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(OTG_FS_OverCurrent_GPIO_Port, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
